package Threads;

public class Multithreading_ByImplementing_ThreadClass {

	public static void main(String[] args) 
	{
		FirstClassSleep_ByImplementingRunnable f=new FirstClassSleep_ByImplementingRunnable();
		SecondClassSleep_ByImplementingRunnable s=new SecondClassSleep_ByImplementingRunnable();
		ThirdClassSleep_ByImplementingRunnable t=new ThirdClassSleep_ByImplementingRunnable();
		Thread t1=new Thread(f);
		Thread t2=new Thread(s);
		Thread t3=new Thread(t);
		t1.start();
		t2.start();
		t3.start();
    }

}
