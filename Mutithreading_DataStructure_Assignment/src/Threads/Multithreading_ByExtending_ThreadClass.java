package Threads;

public class Multithreading_ByExtending_ThreadClass extends Thread 
{
	public static void main(String[] args) 
	{
		FirstClassSleep_ByExtendingThread f=new FirstClassSleep_ByExtendingThread();
		SecondClassSleep_ByExtendingThread s=new SecondClassSleep_ByExtendingThread();
		ThirdClassSleep_ByExtendingThread t=new ThirdClassSleep_ByExtendingThread();
		f.start();
		s.start();
		t.start();
	}
}
