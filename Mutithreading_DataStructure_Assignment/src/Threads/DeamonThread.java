package Threads;

public class DeamonThread extends Thread
{
	public void run()
	{
		if(Thread.currentThread().isDaemon())
		{
			System.out.println("Deamon Thread...");
			System.out.println("-----------------------------");
		}
		else
		{
			System.out.println("User thread...");
			System.out.println("-----------------------------");
		}
	}
	public static void main(String args[])
	{
		DeamonThread d=new DeamonThread();
		DeamonThread d1=new DeamonThread();
		d.setDaemon(true);
	    d.start();
		d1.setDaemon(false);
		d1.start();
	}
}
