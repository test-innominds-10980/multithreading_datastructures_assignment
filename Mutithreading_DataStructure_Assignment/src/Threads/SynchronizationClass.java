package Threads;

public class SynchronizationClass 
{
	synchronized void even()
	{
		for(int i=0;i<10;i++)
		{
			if(i%2==0)
			{
				System.out.print(i+" ");
			}
		}
	}
	synchronized void odd()
	{
		for(int i=0;i<10;i++)
		{
			if(i%2!=0)
			{
				System.out.print(i+" ");
			}
		}
	}
}
