package Threads;

public class ProducerClass implements Runnable 
{
	private ProducerGetAndSetClass p;
	public ProducerClass(ProducerGetAndSetClass p)
	{
		this.p=p;
		Thread t=new Thread(this,"ProducerThread");
		t.start();
	}
	@Override
	public void run() 
	{
		int i=0;
		while(true)
		{
			try
			{
				p.set(i++);
				Thread.sleep(500);
			}
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
		}
	}

}
