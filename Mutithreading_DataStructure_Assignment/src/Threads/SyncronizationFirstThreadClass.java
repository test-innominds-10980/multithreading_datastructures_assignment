package Threads;
public class SyncronizationFirstThreadClass extends Thread
{
	SynchronizationClass s;
	SyncronizationFirstThreadClass(SynchronizationClass s)
	{
		this.s=s;
	}
	public void run(int a,String b)
	{
		try 
		{
			Thread.sleep(a);
			s.even();
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		System.out.println();
		System.out.println(b);
	}
}
