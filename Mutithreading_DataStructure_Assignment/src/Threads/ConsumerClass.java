package Threads;

public class ConsumerClass implements Runnable
{
	private ProducerGetAndSetClass p;
	public ConsumerClass(ProducerGetAndSetClass p)
	{
		this.p=p;
		Thread t=new Thread(this,"ConsumerThread");
		t.start();
	}
	@Override
	public void run() 
	{
		while(true)
		{
			try 
			{
				p.get();
				Thread.sleep(1000);
			}
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
		}
	}
	
}
