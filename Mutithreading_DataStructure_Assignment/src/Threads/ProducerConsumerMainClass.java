package Threads;

public class ProducerConsumerMainClass 
{
	public static void main(String[] args) 
	{
		ProducerGetAndSetClass p=new ProducerGetAndSetClass();
		new ProducerClass(p);
		new ConsumerClass(p);
	}

}
