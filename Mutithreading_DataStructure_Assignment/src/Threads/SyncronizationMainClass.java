package Threads;

public class SyncronizationMainClass 
{
	public static void main(String[] args) 
	{
		SynchronizationClass s=new SynchronizationClass();
		SyncronizationFirstThreadClass s1=new SyncronizationFirstThreadClass(s);
		SyncronizationSecondThreadClass s2=new SyncronizationSecondThreadClass(s);
		int a=Integer.parseInt(args[0]);
		int b=Integer.parseInt(args[1]);
		s1.run(a,args[2]);
		s2.run(b,args[3]);
	}

}
