package Threads;

public class ThreadWithPriority extends Thread
{
	public void run()
	{
		try
		{
			if(t.getPriority()>8)
			{
				t.sleep(1000);
			}
			System.out.println(t.currentThread().getName()+" is alive "+t.isAlive());
			while(t.isAlive() && count!=2)
			{
				System.out.println(t.getName()+" counted to "+count);
				count++;
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	Thread t;
	String name;
	int priority;
	int count=0;
	ThreadWithPriority(String name,int priority)
	{
		this.name=name;
		this.priority=priority;
		t=new Thread(this,name);
		t.setPriority(priority);
		System.out.println(t.getName()+" "+"Thread Started");
		t.start();
		
	}
}
