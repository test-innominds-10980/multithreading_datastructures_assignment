package Threads;

public class FirstClassSleep_ByImplementingRunnable implements Runnable
{
	public void run()
	{
		for(int i=1;i<=10;i++)
		{
			try
			{
				Thread.sleep(1000);
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
			System.out.println("Good Morning");
		}
	}
}
