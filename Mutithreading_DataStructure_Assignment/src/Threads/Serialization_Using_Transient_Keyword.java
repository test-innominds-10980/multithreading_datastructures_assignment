package Threads;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Serialization_Using_Transient_Keyword 
{
	public static void main(String[] args) 
	{
		try
		{
			SerializationDemo s=new SerializationDemo("Prathyusha",10,20);
			FileOutputStream f=new FileOutputStream("C:\\Users\\psale\\Desktop\\Prathyusha.txt");
			ObjectOutputStream o=new ObjectOutputStream(f);
			o.writeObject(s);
			o.flush();
			f.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		try
		{
			FileInputStream f=new FileInputStream("C:\\Users\\psale\\Desktop\\Prathyusha.txt");
			ObjectInputStream o=new ObjectInputStream(f);
			SerializationDemo s=(SerializationDemo)o.readObject();
			System.out.println(s.a+" "+s.b+" "+s.c);
			o.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
