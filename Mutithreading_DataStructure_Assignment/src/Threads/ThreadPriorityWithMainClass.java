package Threads;

public class ThreadPriorityWithMainClass 
{
	public static void main(String[] args)
	{
		ThreadWithPriority t1=new ThreadWithPriority("first",10);
		ThreadWithPriority t2=new ThreadWithPriority("second",9);
		ThreadWithPriority t3=new ThreadWithPriority("Third",5);
		ThreadWithPriority t4=new ThreadWithPriority("fourth",3);
		ThreadWithPriority t5=new ThreadWithPriority("fifth",2);
	}

}
