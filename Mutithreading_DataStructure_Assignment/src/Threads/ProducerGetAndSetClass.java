package Threads;

public class ProducerGetAndSetClass
{
	int i;
	boolean b=false;
	
	public synchronized void set(int i) throws InterruptedException
	{
		while(b)
		{
			wait();
		}
		this.i=i;
		b=true;
		System.out.println("Producer "+i);
		notify();
	}
	public synchronized void get() throws InterruptedException
	{ 
		while(!b)
		{
			wait();
		}
		b=false;
		System.out.println("Consumer "+i);
		notify();
	}
}
