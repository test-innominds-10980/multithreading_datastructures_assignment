package DataStructure;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class LinkedListOfEvenNumbers 
{
	List<Integer> a1=new LinkedList<Integer>();	
	List<Integer> a2=new LinkedList<Integer>();	
	public void saveEvenNumbers(int n)
	{
		for(int i=0;i<n;i++)
		{
			if(i%2==0)
			{
				a1.add(i);
			}
		}
	}
	public LinkedList<Integer> saveEvenNumbers()
	{
		return (LinkedList<Integer>) a1;
	}
	public LinkedList<Integer> printEvenNumbers()
	{
		for(int i=1;i<a1.size();i++)
		{
			int b =(a1.get(i))*2;
			a2.add(b);
		}
		Iterator r=a2.iterator();
		while(r.hasNext())
		{
			System.out.print(r.next()+"\t");
		}
		return (LinkedList<Integer>) a2;
	}
	public int printEvenNumber(int n)
	{
		for(int i=0;i<a2.size();i++)
		{
			if(n==a2.get(i))
			{
				return n;
			}
			else
			{
				return 0;
			}
		}
		return n;
	}
	public static void main(String args[])
	{
		LinkedListOfEvenNumbers a=new LinkedListOfEvenNumbers();
		a.saveEvenNumbers(8);
		System.out.println(a.saveEvenNumbers());
		a.printEvenNumbers();
		System.out.println();
		System.out.println(a.printEvenNumber(4));
	}
}

