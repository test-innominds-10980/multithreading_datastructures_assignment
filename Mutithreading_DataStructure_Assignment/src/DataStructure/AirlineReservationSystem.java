package DataStructure;

import java.util.Scanner;

public class AirlineReservationSystem 
{
	boolean[] flightSeats =new boolean[10];
	Scanner s=new Scanner(System.in);
	public void start()
	{
		while(true)
		{
			bookSeat();
		}
	}
	public void bookSeat()
	{
		System.out.println("Choose your preference: type '1' for Smoking area or type '2' for non-Smoking area or type '0' for exit:");
		String passengerType = s.next();
		if(passengerType.equals("1"))
		{
			SmokingAreaBooking();
		}
		else if(passengerType.equals("2"))
		{
			NonSmokingAreaBooking();
		}
		else if(passengerType.equals("0"))
		{
			System.out.println("Thank you for your response...");
			System.exit(0);
		}
		else
		{
			System.out.println("Invalid Prefernce type.");
			System.exit(0);
		}
	}
	public void SmokingAreaBooking()
	{
		for(int i=0;i<5;i++)
		{
			if(flightSeats[i]==false)
			{
				flightSeats[i]=true;
				System.out.println("Smoking area seat is booked Seat no: "+(i+1));
				break;
			}
			else if(flightSeats[4]==true)
			{
				if(flightSeats[9]==true)
				{
					System.out.println("Sorry all seats are booked, Next flight leaves in 3 hours.");
					break;
				}
				else
				{
					System.out.println("Sorry! Smoking area seats are over. Would you like to go for non Smoking area? press '1' for yes or press '2' for no or type '0' for exit:");
					String selection=s.next();
					if(selection.contains("1"))
					{
						NonSmokingAreaBooking();
					}
					else if(selection.contains("0"))
					{
						System.exit(0);
					}
					else if(selection.contains("2"))
					{
						System.out.println("Next flight leaves in 3 hours.");
						System.exit(0);
					}
					else
					{
						System.out.println("Invalid Prefernce type.");
						System.exit(0);
					}
				}
			}
		}
	}
	public void NonSmokingAreaBooking()
	{
		for(int i=5;i<10;i++)
		{
			if(flightSeats[i]==false)
			{
				flightSeats[i]=true;
				System.out.println("Non Smoking area seat is booked seat no:"+(i+1));
				break;
			}
			else if(flightSeats[9]==true)
			{
				if(flightSeats[4]==true)
				{
					System.out.println("Sorry! All seats are booked, Next flight leaves in 3 hours. ");
					break;
				}
				else
				{
					System.out.println("Sorry! Non Smokking area seats are booked, Would you like to go for Smoking area? press '1' for yes or press '2' for no or type '0' for exit:");
					String selection=s.next();
					if(selection.contains("1"))
					{
						SmokingAreaBooking();
					}
					else if(selection.contains("0"))
					{
						System.exit(0);
					}
					else if(selection.contains("2"))
					{
						System.out.println("Next flight leaves in 3 hours.");
						System.exit(0);
					}
					else
					{
						System.out.println("Invalid Prefernce type.");
						System.exit(0);
					}
				}
			}
		}
	}
	public static void main(String args[])
	{
		AirlineReservationSystem a=new AirlineReservationSystem();
		a.start();
	}
}
