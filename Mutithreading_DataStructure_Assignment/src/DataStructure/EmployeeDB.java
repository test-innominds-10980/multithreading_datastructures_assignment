package DataStructure;

import java.util.ArrayList;
import java.util.Iterator;

public class EmployeeDB 
{
	ArrayList<Employee> a=new ArrayList<Employee>();
	public boolean addEmployee(Employee e)
	{
		return a.add(e);	
	}
	public boolean deleteEmployee(int empId)
	{
		boolean removed=true;
		Iterator<Employee> i=a.iterator();
		while(i.hasNext())
		{
			Employee e=i.next();
			if(e.empid==empId)
			{
				removed=false;
				i.remove();
			}
		}
		return removed;	
	}
	public String showPaySlip(int empId)
	{
		String paySlip="Invalid paySlip";
		for(Employee e:a)
		{
			if(e.empid==empId)
			{
				return "PaySlip for "+empId+" is "+e.empSalary;
			}
		}
		return paySlip;	
	}
	public Employee[] listAll()
	{
		Employee[] e=new Employee[a.size()];
		for(int i=0;i<a.size();i++)
		{
			e[i]=a.get(i);
		}
		return e;		
	}
}
