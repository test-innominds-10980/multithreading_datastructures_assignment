package DataStructure;

public class FourCoreInterfaces 
{
/*	Consider the four core interfaces, Set, List, Queue, and Map. For each of the following four 
	assignments, specify which of the four core interfaces is best-suited, and explain how to use it to 
	implement the assignment.
	
	a. Whimsical Toys Inc (WTI) needs to record the names of all its employees. Every month, an 
	employee will be chosen at random from these records to receive a free toy.
	
	b. WTI has decided that each new product will be named after an employee but only first names 
	will be used, and each name will be used only once. Prepare a list of unique first names.
	
	c. WTI decides that it only wants to use the most popular names for its toys. Count up the 
	number of employees who have each first name.
	
	d. WTI acquires season tickets for the local lacrosse team, to be shared by employees. Create a 
	waiting list for this popular sport.
*/
	
	
	
	//Answers
	/*
	 * a.  List is most suitable for option a, because It can choose a random employee from 0to size()-1
	 * 
	 * b.  Set is more suitable for option b, because Collections that implement this interface don't allow same element to be entered more than once.
	 * 
	 * c.  Map is  more suitable for option c, because where the keys are first names, and each value is a count of the number of employees with that first name.
	 * 
	 * d.  Queue is more suitable for option d, because Invoke add() to add employees to the waiting list, and remove() to remove them.
	 */
}
