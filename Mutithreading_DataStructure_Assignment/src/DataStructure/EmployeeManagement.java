package DataStructure;

public class EmployeeManagement 
{
	public static void main(String[] args) 
	{
		EmployeeDB e=new EmployeeDB();
		Employee one=new Employee(1,"Prathyusha","prathyusha@gmail.com",'f',20000);
		Employee two=new Employee(2,"Pravallika","pravallika@gmail.com",'f',30000);
		Employee three=new Employee(3,"Madhu","madhu@gmail.com",'m',34000);
		Employee four=new Employee(4,"Suvarna","suvarna@gmail.com",'f',44000);
		Employee five=new Employee(5,"Indira","indira@gmail.com",'f',15000);
		e.addEmployee(one);
		e.addEmployee(two);
		e.addEmployee(three);
		e.addEmployee(four);
		e.addEmployee(five); 
		for(Employee e1:e.listAll())
		{
			System.out.println(e1.GetEmployeeDetails());
		}
		System.out.println();
		e.deleteEmployee(5);
		for(Employee e1:e.listAll())
		{
			System.out.println(e1.GetEmployeeDetails());
		}
		System.out.println();
		System.out.println(e.showPaySlip(3));
	}
}
